

/*
 * Name:Sam Whelan
 * Student Number: 991541458
 * Date: February 3rd 2021
 * Description: This class will be a password validator module which will test whether the user entered password has at least 8 characters and 
 * at least 2 digits
 * */
public class Validator {
	
	private static int MIN_LENGTH = 8;
	private static int MIN_DIGITS = 2;

	/**
	 * Checks to confirm if the user entered password is at least 8 characters and not empty
	 * @param pswd the users entered password
	 * @return a boolean based on whether or not the password is a valid length
	 */
	public static boolean isValidLength(String pswd) {
		//Checks if the password is not null, as well makes sure it meets the min length, if it does returns true
		if(pswd != null && pswd.length() >= MIN_LENGTH) {
			return true;
		//If less than the min length or null will return false
		}else {
			return false;
		}
			
	}	
	
	/**
	 * This method checks to determine if the password entered has at least 2 digits in it
	 * @param pswd the password that the user enters to be verified
	 * @return a boolean checking whether or not the password contains at least 2 digits
	 */
	public static boolean hasTwoDigits(String pswd) {	
		int counter = 0;
		boolean answer = false;
		//If the password is not null, check if it contains at least 2 digits
		if(pswd != null){
			//Convert the password to an array of characters
			char[]letters = pswd.toCharArray();
			//Search through the array to check whether or not there are digits, if there are teh counter increases. 
			//If there are at least 2, it will return true
			for(char c : letters) {
				if(Character.isDigit(c)) {
					counter++;
				}
			}
			if(counter >= 2) {
				answer = true;
			}
		}
		return answer;
		
	}
	
	public static boolean hasValidCaseCharacters(String pswd) {
		if(pswd != null) {
			return pswd.matches(".*[A-Z]+.*") && pswd.matches(".*[a-z]+.*");
		}else {
			return false;
		}
		
	}
	
		
}
